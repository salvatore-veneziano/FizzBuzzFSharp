﻿
let (<+>) fa fb a =
        match fa a, fb a with
        | Some a, Some b -> Some(a + b)
        | Some a, _ -> Some a
        | _, Some b -> Some b
        | _,_ -> None

let (<??>) fa fdef a = defaultArg (fa a) (fdef a)

let rule modulo output x = if x % modulo = 0 then Some output else None

[<EntryPoint>]
let main _ =  

    let fizz = rule 3 "Fizz"
    let buzz = rule 5 "Buzz"
    let bang = rule 7 "Bang"
    let echo = string

    let fizzBuzzBang = fizz <+> buzz <+> bang <??> echo

    [1..200] |> List.iter (fizzBuzzBang >> printfn "%A")
    0
